import './style/main.css'
import adblockDetection from 'theme-library/module/adblock-detection'
import dataClipboard from 'theme-library/module/data-clipboard'
import dataLink from 'theme-library/module/data-link'
import dataSearch from 'theme-library/module/data-search'
import dataToggle from 'theme-library/module/data-toggle'
import utmBuilder from 'theme-library/module/utm-builder'

const head = document.querySelector('head')
const body = document.querySelector('body')
window.addEventListener('load', () => {
  adblockDetection(head, body)
  utmBuilder()
})
body.addEventListener('click', (event) => {
  dataClipboard(event)
  dataLink(event)
  dataToggle(event)
})
body.addEventListener('submit', dataSearch)
