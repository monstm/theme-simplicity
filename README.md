# Theme Simplicity

## Pages

* [Index Page](https://monstm.gitlab.io/theme-simplicity/html/index.html)
* [List Page](https://monstm.gitlab.io/theme-simplicity/html/list-page.html)
* [Single Page](https://monstm.gitlab.io/theme-simplicity/html/single-page.html)
* [Error Document](https://monstm.gitlab.io/theme-simplicity/html/404.html)
