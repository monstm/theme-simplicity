const fs = require('fs')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { LoremIpsum } = require('lorem-ipsum')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const path = require('path')

const isProduction = !!process.env.CI_COMMIT_TAG

const imageAssets = ['.webp', '.svg', '.png', '.jpg', '.jpeg', '.gif', '.ico']
const fontAssets = ['.ttf', '.otf', '.eot', '.woff', '.woff2']
const assetResources = [].concat(imageAssets, fontAssets)

const sourceDirectory = path.resolve(__dirname, 'src')
const distributionDirectory = path.resolve(__dirname, 'dist')

const listPageLink = 'list-page.html'
const singlePageLink = 'single-page.html'
const timeInformation = 'Oct 4, 2015'
const datetimeInformation = '2015-10-04 20:00'

const getHtmlPlugins = (sourceDirectory) => {
  const htmlPlugins = []
  const twigDirectory = path.resolve(sourceDirectory, 'twig')

  const files = fs.readdirSync(twigDirectory)
  const twigFiles = files.filter(
    (file) => path.extname(file).toLowerCase() === '.twig'
  )

  const lorem = new LoremIpsum()
  const templateParameters = {
    isProduction,
    theme: {
      name: 'Simplicity',
      tagline: 'everything you need for your blog',
      link: 'https://gitlab.com/monstm/theme-simplicity',
    },
    leaderboard: leaderboardFactory(),
    navbarTop: navbarFactory('Top Navbar', 4, true),
    navbarMiddle: navbarFactory('Middle Navbar', 6),
    navbarBottom: navbarFactory('Bottom Navbar', 5),
    asideOffers: asideOffersFactory(2),
    asideFeeds: asideFeedsFactory(3),
    asideList: asideListFactory(6),
    asideTags: asideTagsFactory(20),
    footerColumns: footerColumnsFactory(3),
    post: postFactory(),
    postlist: feedItemsFactory(10, 150, 150, true),
    loremWords: lorem.generateWords(),
    loremSentences: lorem.generateSentences(),
    loremParagraphs: lorem.generateParagraphs(1),
  }

  twigFiles.forEach((twigFile) => {
    const twigName = path.parse(twigFile).name
    const template = path.resolve(twigDirectory, twigFile)
    const filename = `html/${twigName}.html`

    htmlPlugins.push(
      new HtmlWebpackPlugin({
        template,
        filename,
        templateParameters,
      })
    )
  })

  return htmlPlugins
}

const leaderboardFactory = () => {
  return {
    desktop: {
      image: 'https://placehold.co/728x90.webp',
      width: 728,
      height: 90,
    },
    mobile: {
      image: 'https://placehold.co/320x50.webp',
      width: 320,
      height: 50,
    },
  }
}

const navbarFactory = (ariaLabel, itemSize, withShortcuts = false) => {
  const navbar = {
    ariaLabel,
    items: textLinkItemsFactory(itemSize, 2, listPageLink),
  }

  if (withShortcuts) {
    navbar.shortcuts = [
      { text: 'Twitter', icon: 'icon-twitter' },
      { text: 'Facebook', icon: 'icon-facebook' },
      { text: 'LinkedIn', icon: 'icon-linkedin' },
      { text: 'YouTube', icon: 'icon-youtube' },
      { text: 'Tumblr', icon: 'icon-tumblr' },
      { text: 'Pinterest', icon: 'icon-pinterest' },
    ]
  }

  return navbar
}

const asideOffersFactory = (size) => {
  const items = []

  for (let index = 0; index < size; index++) {
    items.push({
      title: `Offer ${index + 1}`,
      image: 'https://placehold.co/300x250.webp',
      width: 300,
      height: 250,
    })
  }

  return {
    items,
  }
}

const asideFeedsFactory = (size) => {
  return {
    title: 'Feeds Section',
    items: feedItemsFactory(size, 100, 100),
  }
}

const asideListFactory = (size) => {
  return {
    title: 'List Section',
    items: textLinkItemsFactory(size, 6, singlePageLink),
  }
}

const asideTagsFactory = (size) => {
  return {
    title: 'Tags Section',
    items: textLinkItemsFactory(size, 3, listPageLink),
  }
}

const footerColumnsFactory = (size) => {
  const columns = []

  for (let index = 0; index < size; index++) {
    columns.push({
      title: `Column ${index + 1}`,
      items: textLinkItemsFactory(5, 4, singlePageLink),
    })
  }

  columns.push({
    title: 'Pages',
    items: [
      {
        text: 'Index Page',
        link: 'index.html',
      },
      {
        text: 'List Page',
        link: 'list-page.html',
      },
      {
        text: 'Single Page',
        link: 'single-page.html',
      },
      {
        text: '404 Page',
        link: '404.html',
      },
    ],
  })

  return columns
}

const postFactory = () => {
  const width = 1200
  const height = 630
  const lorem = new LoremIpsum()

  return {
    title: 'Single Page',
    subtitle: lorem.generateSentences(1),
    time: timeInformation,
    datetime: datetimeInformation,
    image: `https://placehold.co/${width}x${height}.webp`,
    width,
    height,
    taxonomies: [
      {
        icon: 'icon-folder',
        items: textLinkItemsFactory(3, 3, listPageLink),
      },
      {
        icon: 'icon-tag',
        items: textLinkItemsFactory(6, 3, singlePageLink),
      },
    ],
  }
}

const textLinkItemsFactory = (size, words, link) => {
  const items = []
  const lorem = new LoremIpsum()

  for (let index = 0; index < size; index++) {
    items.push({
      text: lorem.generateWords(random(words)),
      link,
    })
  }

  return items
}

const feedItemsFactory = (size, width, height, withDescription = false) => {
  const feeds = []
  const lorem = new LoremIpsum()

  for (let index = 0; index < size; index++) {
    const feed = {
      title: lorem.generateSentences(1),
      time: timeInformation,
      datetime: datetimeInformation,
      image: `https://placehold.co/${width}x${height}.webp`,
      width,
      height,
      link: singlePageLink,
    }

    if (withDescription) {
      feed.description = lorem.generateParagraphs(1)
    }

    feeds.push(feed)
  }

  return feeds
}

const random = (max = 9) => {
  return Math.floor(Math.random() * max + 1)
}

const hasExtension = (filename, extentions) => {
  const extname = path.extname(filename)
  const extension = extname.split('?')[0]
  return extentions.includes(extension)
}

let mode = 'production'
let outputFilename = 'main.js'
let cssFilename = 'main.css'
let assetFilename = '[name][ext]'

if (!isProduction) {
  mode = 'development'
  outputFilename = 'main-[contenthash].js'
  cssFilename = 'main-[contenthash].css'
  assetFilename = '[name]-[contenthash][ext]'
}

const plugins = [
  new MiniCssExtractPlugin({
    filename: cssFilename,
  }),
  new FaviconsWebpackPlugin({
    logo: 'src/image/favicon.webp',
    prefix: 'image/',
    favicons: {
      icons: {
        android: false,
        appleIcon: false,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: false,
        windows: false,
        yandex: false,
      },
    },
  }),
].concat(getHtmlPlugins(sourceDirectory))

module.exports = {
  mode,
  entry: './src/entry.js',
  output: {
    path: distributionDirectory,
    filename: outputFilename,
    assetModuleFilename: (params) => {
      let assetDiretory = 'asset'

      if (hasExtension(params.filename, imageAssets)) {
        assetDiretory = 'image'
      }

      if (hasExtension(params.filename, fontAssets)) {
        assetDiretory = 'font'
      }

      return `${assetDiretory}/${assetFilename}`
    },
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader'],
      },
      {
        test: /\.twig$/,
        use: ['twig-loader'],
      },
      {
        test: new RegExp(assetResources.join('|') + '$', 'i'),
        type: 'asset/resource',
      },
    ],
  },
  plugins,
  devServer: {
    static: {
      directory: distributionDirectory,
    },
    compress: true,
    port: 9000,
  },
  resolve: {
    fallback: {
      fs: false,
    },
  },
  devtool: false,
}
