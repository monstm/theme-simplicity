/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: ['./src/**/*.{html,twig}'],
  theme: {
    extend: {
      fontFamily: {
        wayland: ['Wayland', ...defaultTheme.fontFamily.sans],
        durusans: ['Durusans', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
